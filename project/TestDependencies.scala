import sbt._

object TestVersions {

  final val circe     = "0.13.0"
  final val http4s    = "0.21.1"
  final val requests  = "0.2.0"
  final val scalatest = "3.0.8"
  final val tapir     = "0.12.23"
  final val upickle   = "0.8.0"
  final val wiremock  = "2.25.1"

}

object TestDependencies {

  object Circe {

    final val core          = "io.circe" %% "circe-core"           % TestVersions.circe % Test
    final val generic       = "io.circe" %% "circe-generic"        % TestVersions.circe % Test
    final val genericExtras = "io.circe" %% "circe-generic-extras" % TestVersions.circe % Test

    final val all = List(core, generic, genericExtras)

  }

  final val http4sCirce    = "org.http4s"                  %% "http4s-circe"     % TestVersions.http4s    % Test
  final val requests       = "com.lihaoyi"                 %% "requests"         % TestVersions.requests  % Test
  final val scalatest      = "org.scalatest"               %% "scalatest"        % TestVersions.scalatest % Test
  final val tapirJsonCirce = "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % TestVersions.tapir     % Test
  final val wiremock       = "com.github.tomakehurst"      % "wiremock"          % TestVersions.wiremock  % Test

}
