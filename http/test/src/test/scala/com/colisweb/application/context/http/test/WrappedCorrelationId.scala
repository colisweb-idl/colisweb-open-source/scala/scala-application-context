package com.colisweb.application.context.http.test

final case class WrappedCorrelationId(correlationId: String)
